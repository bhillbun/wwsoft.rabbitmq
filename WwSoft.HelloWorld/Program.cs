﻿using System;
using System.Windows.Forms;

namespace WwSoft.RabbitMQ.HelloWorld {
    static class Program {

        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }

    }
}
