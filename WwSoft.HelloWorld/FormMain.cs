﻿using System;
using System.Windows.Forms;

namespace WwSoft.RabbitMQ.HelloWorld {
    public partial class FormMain : Form {
        private IRMConnectionProvider m_connectionProvider = default(IRMConnectionProvider);

        public FormMain() {
            InitializeComponent();
        }

        private void SetConnection() {
            if (m_connectionProvider == null)
                m_connectionProvider = new RMSharedConnectionProvider() { 
                    HostName = tbServer.Text.Trim(), 
                    UserName = tbUserName.Text.Trim(), 
                    Password = tbPassword.Text.Trim() 
                };
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            if (m_connectionProvider != null) m_connectionProvider.Dispose();
        }

        private void FormMain_Load(object sender, EventArgs e) {
            cmbxChannelType.SelectedIndex = 0;
        }

        private void btnEnqueue_Click(object sender, EventArgs e) {
            SetConnection();

            if (cmbxChannelType.Text.StartsWith("Persistent Work Queue")) {

                using (var channel = new RMWorkQueueChannel<StringQueueMessage>(m_connectionProvider) { Name = tbChannelName.Text.Trim(), Persist = false }) {
                    channel.EnqueueAsync(
                        () => {
                            return new StringQueueMessage() { Body = tbMessage.Text.Trim() };
                        },
                        () => {
                            return new QueueMessageMetadata() { Persist = false };
                        });

                    tbResponse.Text += String.Format("Persistent Work Queue Sent: {0}{1}", tbMessage.Text.Trim(), System.Environment.NewLine);
                }

            } else if (cmbxChannelType.Text.StartsWith("Non-Persistent Queue")) {

                using (var channel = new RMQueueChannel<StringQueueMessage>(m_connectionProvider) { Name = tbChannelName.Text.Trim(), Persist = false }) {
                    channel.EnqueueAsync(
                        () => {
                            return new StringQueueMessage() { Body = tbMessage.Text.Trim() };
                        },
                        () => {
                            return new QueueMessageMetadata() { Persist = false };
                        });

                    tbResponse.Text += String.Format("Non-Persistent Queue Sent: {0}{1}", tbMessage.Text.Trim(), System.Environment.NewLine);
                }

            } else {
                MessageBox.Show("Please select a valid channel type");
            }
        }

        private void btnDequeue_Click(object sender, EventArgs e) {
            SetConnection();

            if (cmbxChannelType.Text.StartsWith("Persistent Work Queue")) {

                using (var channel = new RMWorkQueueChannel<StringQueueMessage>(m_connectionProvider) { Name = tbChannelName.Text.Trim(), Persist = false }) {
                    btnDequeue.Enabled = false;
                    channel.DequeueAsync((message) => {
                        tbResponse.Invoke((Action)delegate {
                            tbResponse.Text += String.Format("Queue Received: {0}{1}", message.Body, System.Environment.NewLine);
                        });

                        return true;
                    });
                }

            } else if (cmbxChannelType.Text.StartsWith("Non-Persistent Queue")) {

                using (var channel = new RMQueueChannel<StringQueueMessage>(m_connectionProvider) { Name = tbChannelName.Text.Trim(), Persist = false }) {
                    btnDequeue.Enabled = false;
                    channel.DequeueAsync((message) => {
                        tbResponse.Invoke((Action)delegate {
                            tbResponse.Text += String.Format("Queue Received: {0}{1}", message.Body, System.Environment.NewLine);
                        });

                        return true;
                    });
                }

            }
        }
    }
}
