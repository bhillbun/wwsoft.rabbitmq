﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace WwSoft.RabbitMQ {
    public class RMWorkQueueChannel<TheQueueMessage> : RMBaseQueueChannel<TheQueueMessage> where TheQueueMessage : QueueMessage {

        public RMWorkQueueChannel(IRMConnectionProvider connectionProvider) : base(connectionProvider) {
            Persist = false;
            Name = String.Empty;
        }

        public override void Enqueue(Func<TheQueueMessage> messageHandler, Func<QueueMessageMetadata> messageMetadata) {
            using (var channel = ConnectionProvider.Connection.CreateModel()) {
                if (messageHandler == null || messageMetadata == null) return;

                channel.QueueDeclare(Name, Persist, false, false, null);

                var message = messageHandler();
                var metadata = messageMetadata();

                var serializer = new XmlSerializer(message.GetType());
                var writer = new StringWriter();
                serializer.Serialize(writer, message);
                var body = writer.ToString();

                var properties = channel.CreateBasicProperties();
                properties.Headers = new Dictionary<string, object>();
                properties.Headers.Add("x-type", message.GetType().ToString());
                properties.Headers.Add("x-serializer", typeof(XmlSerializer).ToString());
                properties.Headers.Add("x-version", metadata.MessageVersion);
                properties.SetPersistent(Persist && metadata.Persist);

                channel.BasicPublish(String.Empty, Name, properties, Encoding.UTF8.GetBytes(body));
            }
        }

        public override void Dequeue(Func<TheQueueMessage, bool> messageHandler, bool blocking = true) {
            using (var channel = ConnectionProvider.Connection.CreateModel()) {
                channel.QueueDeclare(Name, Persist, false, false, null);
                channel.BasicQos(0, 1, false);

                var consumer = new QueueingBasicConsumer(channel);
                channel.BasicConsume(Name, false, consumer);

                try {
                    while (true) {
                        var ea = (blocking) ? (BasicDeliverEventArgs)consumer.Queue.Dequeue() : (BasicDeliverEventArgs)consumer.Queue.DequeueNoWait(null);
                        if (ea == null) return;

                        var body = ea.Body;
                        var xml = Encoding.UTF8.GetString(body);

                        var serializer = new XmlSerializer(typeof(TheQueueMessage));
                        var reader = new StringReader(xml);
                        var message = serializer.Deserialize(reader);

                        if (messageHandler != null) {
                            if (!messageHandler((TheQueueMessage)message)) {
                                Thread.Sleep(500);
                                continue;
                            }
                        }

                        channel.BasicAck(ea.DeliveryTag, false);
                    }
                } catch (EndOfStreamException) { } catch (AlreadyClosedException) { }
            }
        }
    }
}
