﻿using System;

namespace WwSoft.RabbitMQ {
    public interface IQueueChannel<TheQueueMessage> where TheQueueMessage : QueueMessage {
        bool Persist { get; set; }

        void Enqueue(Func<TheQueueMessage> messageHandler, Func<QueueMessageMetadata> messageMetadata);
        void EnqueueAsync(Func<TheQueueMessage> messageHandler, Func<QueueMessageMetadata> messageMetadata);
        void Dequeue(Func<TheQueueMessage, bool> messageHandler, bool blocking = true);
        void DequeueAsync(Func<TheQueueMessage, bool> messageHandler, bool blocking = true);
    }
}
