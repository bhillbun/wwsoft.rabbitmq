﻿
namespace WwSoft.RabbitMQ {
    public interface IConnectionProvider<TheConnection> {
        TheConnection GetConnection();
    }
}
