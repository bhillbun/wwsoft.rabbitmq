﻿using System;
using RabbitMQ.Client;

namespace WwSoft.RabbitMQ {
    public class RMSharedConnectionProvider : IRMConnectionProvider, IDisposable {
        private IConnection m_connection = default(IConnection);

        public IConnection Connection {
            get {
                return GetConnection();
            }
        }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public RMSharedConnectionProvider() {
            HostName = "localhost";
        }

        public IConnection GetConnection() {
            if (m_connection != default(IConnection) && m_connection.IsOpen) return m_connection;

            var connFactory = new ConnectionFactory() { HostName = HostName, UserName = UserName, Password = Password };

            m_connection = (IConnection)connFactory.CreateConnection();

            return m_connection;
        }

        public void Dispose() {
            if (m_connection != default(IConnection)) {
                if (m_connection.IsOpen)
                    m_connection.Close(5000);
                m_connection.Dispose();
            }
        }
    }
}
