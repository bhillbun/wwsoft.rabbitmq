﻿using System;
using RabbitMQ.Client;

namespace WwSoft.RabbitMQ {
    public interface IRMConnectionProvider : IConnectionProvider<IConnection>, IDisposable {
        IConnection Connection { get; }
        string HostName { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
    }
}
