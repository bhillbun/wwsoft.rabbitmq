﻿using System;
using System.Threading.Tasks;

namespace WwSoft.RabbitMQ {
    public abstract class RMBaseQueueChannel<TheQueueMessage> : IQueueChannel<TheQueueMessage>, IDisposable where TheQueueMessage : QueueMessage {
        protected IRMConnectionProvider ConnectionProvider { get; set; }

        public string Name { get; set; }
        public bool Persist { get; set; }

        public RMBaseQueueChannel(IRMConnectionProvider connectionProvider) {
            ConnectionProvider = connectionProvider;
        }

        public abstract void Enqueue(Func<TheQueueMessage> messageHandler, Func<QueueMessageMetadata> messageMetadata);

        public async void EnqueueAsync(Func<TheQueueMessage> messageHandler, Func<QueueMessageMetadata> messageMetadata) {
            await Task.Run(() => {
                Enqueue(messageHandler, messageMetadata);
            });
        }

        public abstract void Dequeue(Func<TheQueueMessage, bool> messageHandler, bool blocking = true);

        public async void DequeueAsync(Func<TheQueueMessage, bool> messageHandler, bool blocking = true) {
            await Task.Run(() => {
                Dequeue(messageHandler, blocking);
            });
        }

        public void Dispose() {

        }
    }
}
