﻿using System;

namespace WwSoft.RabbitMQ {
    [Serializable]
    public class BinaryQueueMessage : QueueMessage {
        public byte[] Body { get; set; }
    }
}
