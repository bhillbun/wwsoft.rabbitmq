﻿using System;
using System.Collections.Generic;

namespace WwSoft.RabbitMQ {
    [Serializable]
    public class ListQueueMessage<T> : QueueMessage {
        public List<T> Body { get; set; }
    }
}
