﻿
namespace WwSoft.RabbitMQ {
    public class QueueMessageMetadata {
        public string MessageVersion { get; set; }
        public bool Persist { get; set; }
    }
}
