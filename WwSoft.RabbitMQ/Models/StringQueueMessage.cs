﻿using System;

namespace WwSoft.RabbitMQ {
    [Serializable]
    public class StringQueueMessage : QueueMessage {
        public string Body { get; set; }
    }
}
